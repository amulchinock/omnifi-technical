import http from 'http'
import Router from 'node-router'
import axios from 'axios'
import cheerio from 'cheerio'

let router = new Router()

router.push('*', '/', (request, response, next) => {
  axios.get('https://en.wikipedia.org/wiki/Special:Random')
  .then((axiosResponse) => {
    const $ = cheerio.load(axiosResponse.data)
    let responseObject = {
      pageUrl: axiosResponse.request.res.responseUrl,
      pageTitle: $('h1').text(),
      image: $('table.infobox tbody tr:nth-of-type(2) td a img').attr('src'),
      firstParagraph: $('.mw-parser-output > p:not(.mw-empty-elt)').html(),
      categories: (() => {
        let categories = []
        $('#mw-normal-catlinks ul li').each((i, element) => {
          const listItem = cheerio.load($(element).html())
          categories.push({
            categoryName: listItem('a').text(),
            categoryUrl: listItem('a').attr('href')
          })
        })

        return categories
      })()
    }

    response.send(responseObject)
  })
  .catch((axiosError) => {
    response.send(axiosError)
  })
})

const port = process.env.PORT || 8080
const server = http.createServer(router).listen(port)
