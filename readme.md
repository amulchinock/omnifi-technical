# Hello

## API endpoint
The API is hosted on AWS Elastic Beanstalk.
You can interact with the enpoint here: http://omnifi-technical.eu-west-2.elasticbeanstalk.com/

## Spinning up
**Warning: Please ensure you do not already have an instance of localhost running on port 8080 on your local machine, prior to spinning up this project.**

To get this project working locally, simply do the following:

  - Open a terminal window

  - Navigate to the route directory of this project ``` cd /omnifi-technical```

  - Run ```npm install``` - this could take a few minutes. Grab a coffee, get some fresh air .etc

  - Run ```npm run dev```

The API server will now be running locally on your machine at ```http://localhost:8080```

## Project structure
All application logic is located within index.js (asside from any logic handled by dependancies).

When starting out on this - I initially thought that I may need to create additional classes or utility methods to handle the parsing of the scraped content.  However, this proved unecessary, as there was very little going on in the completed response code - and I felt that splitting out a codebase this small would actually overcomplicate things at this stage.

There would obviously be an exception to this: if I had been using a test driven development approach. In that case, it likely would have been necessary to write each aspect of functionality in it's own testable method.

## Assumptions
  - We don't care about what type of request, or what route is being used to
  contact our API. The API will fetch from Wikipedia regardless

  - The API will ignore any information sent to it

  - HTML (anchor tags .etc) are left in firstParagraph. This helps
  to avoid the issue of having to remove footnotes (e.g [3]) from the response

  - Wikipedia's hidden (internal) categories (e.g "Articles with unsourced statements from October 2012") are not included in the array of page categories

## Dependancies
  - esm - Handy way of transpiling ES6 down to ES5, without using Babel
  - node-router - Simple routing library
  - axios - Promise based HTTP client for the browser and node.js
  - cheerio - jQuery for Node.js. Cheerio makes it easy to select, edit, and view DOM elements.
  - nodemon - Simple dev server that automatically restarts when it detects code changes
